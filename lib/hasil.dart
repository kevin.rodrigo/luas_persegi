import 'package:flutter/material.dart';

class Hasil extends StatelessWidget {
  @override

  // ignore: override_on_non_overriding_member
  final double luas;
  final double keliling;
  final double panjang;
  final double lebar;

  Hasil({this.luas, this.keliling, this.panjang, this.lebar});

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('HASIL'),
      ),
      body: Center(
        child: Container(
          margin: EdgeInsets.only(top: 10, left: 90, right: 90),
          height: 300,
          width: 500,
          decoration: BoxDecoration(
              color: Colors.blue, borderRadius: BorderRadius.circular(10)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                margin: EdgeInsets.all(2),
                decoration: BoxDecoration(
                    color: Colors.black,
                    borderRadius: BorderRadius.circular(10)),
                child: Center(
                  child: (Text(
                    'HASIL',
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 20),
                  )),
                ),
              ),
              Padding(padding: EdgeInsets.all(10.0)),
              Container(
                margin: EdgeInsets.all(2),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Text("Nilai Panjang yang anda masukan =   ",
                            style: TextStyle(color: Colors.white)),
                        Text("$panjang cm ",
                            style: TextStyle(color: Colors.black)),
                      ],
                    ),
                    Row(
                      children: [
                        Text("Nilai Lebar yang anda masukan =  ",
                            style: TextStyle(color: Colors.white)),
                        Text("$lebar cm   ",
                            style: TextStyle(color: Colors.black)),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(padding: EdgeInsets.all(10.0)),
              Container(
                child: Column(
                  children: [
                    Text("  LUAS =  ",
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold)),
                    Text("$luas cm^2",
                        style: TextStyle(color: Colors.black, fontSize: 50))
                  ],
                ),
              ),
              Padding(padding: EdgeInsets.all(10)),
              Container(
                child: Column(
                  children: [
                    Text("  KELILING =  ",
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold)),
                    Text("$keliling cm",
                        style: TextStyle(color: Colors.black, fontSize: 50))
                  ],
                ),
              )
            ],
          ),
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        child: Container(
          height: 40,
          color: Colors.black54,
          alignment: Alignment.center,
          child: Text(
            "Copyright @2021",
            style: TextStyle(fontSize: 15, color: Colors.grey),
          ),
        ),
      ),
    );
  }
}
