import 'package:flutter/material.dart';
import 'package:luas_persegi/profil.dart';
import 'hasil.dart';

// ignore: camel_case_types
class app_persegi extends StatefulWidget {
  @override
  _app_persegiState createState() => _app_persegiState();
}

double _panjang = 0;
double _lebar = 0;
double _luas = 0;
double _keliling = 0;

// ignore: camel_case_types
class _app_persegiState extends State<app_persegi> {
  @override
  Widget build(BuildContext context) {
    var scaffold = Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: Icon(
          Icons.menu,
        ),
        title: Text('APLIKASI MATEMATIKA'),
        actions: [
          IconButton(
              icon: Icon(Icons.info_outline),
              iconSize: 30,
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => Profil()),
                );
              })
        ],
      ),
      body: Container(
        margin: EdgeInsets.all(10),
        child: Column(
          children: <Widget>[
            Text(
              "Menghitung Luas & Keliing Persegi Panjang",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                  color: Colors.white),
            ),
            Padding(padding: EdgeInsets.all(15)),
            Container(
              child: Image.network(
                "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBIREg8UEg8SEQ8RDxEREg8PFRoRERIRFxQaGRkVGBgcLi8mHCw4IxYYJzgmLDAxNTU1HDFIQEA4Py9CQzEBDAwMEA8QHxISHzYrJCQxPzExNDQ/MTE0PTE0MTQ0MTE0NDE/NDcxNDE0NDQxNEA0NDQ4MTQxMTwxMT8xNDQ0P//AABEIAMIBAwMBIgACEQEDEQH/xAAcAAEAAwADAQEAAAAAAAAAAAAAAQQGAgUHAwj/xAA/EAABBAEBBQQHBQYFBQAAAAAAAQIDBBEFEhYhVNEGFZGTEyIxQVF0oQc0NbGyFDJCYXGBIyUzUvAkRGJykv/EABgBAQEBAQEAAAAAAAAAAAAAAAAEAgMB/8QAKREBAAAEBAQHAQEAAAAAAAAAAAECEVEDBJHRI2FxsRIxMjNBcoHwIf/aAAwDAQACEQMRAD8A9mAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAUbGqV45GRSWImTSf6cUj2tkk44TYYq5dx+CFxFyfn/tdZZbuancbcjgnoTQx04XOYx8yQuckiIiLlePrNcirlOGMr6vt3Z/UW26taducTQsfhcZRVamUXCqicc8AO0BGRkCQRkZAkEZGQJBGRkCQRkZAkEZGQJBGRkCQRkZAkEZGQJBGRkCQRkZAkEZGQJBGRkCT4WbDImOfI9scbEVzpJFRrGtT2q5y8EQ+2TO9v1/yvUvlJf0gXKvaOjM9rIr9SSR6qjY4543vdhFVcNauV4Iq/2O1auT86JHJ3XpEktV0FGG1K9+o1HMfce700myjWqrdjCorUVVXiiKnswv6HryNe1rmKjmOajmuRco5qplFRf7gfYAAZjs/2Ur06f7Hj08a+kSR0zW5ej854exEx/U+3ZLQu7a37M2d8zGSSOjV7WtdGx652FVv73rK5cr/u+CIaDBwcgFGXWKzHOa6zC1zfa10jEci/BUzwI78qc3X85nUyHZ7Ra9qxqqzxNlVl6VrVcq+qm27gmPaaJOyGn8ozxd1OUIzxhWCufCy+HN4ZozV6Qt1Xu+6nN1/OZ1HfdTm6/nM6lLdDT+UZ4u6jdDT+UZ4u6nvE5M0y15tIbrvfdTm6/nM6jvupzdfzmdSluhp/KM8XdRuhp/KM8XdRxORTLXm0huu991Obr+czqO+6nN1/OZ1KW6Gn8ozxd1G6Gn8ozxd1HE5FMtebSG6733U5uv5zOo77qc3X85nUpboafyjPF3UboafyjPF3UcTkUy15tIbrvfdTm6/nM6jvupzdfzmdSluhp/KM8XdRuhp/KM8XdRxORTLXm0huu991Obr+czqO+6nN1/OZ1KW6Gn8ozxd1G6Gn8ozxd1HE5FMtebSG6733U5uv5zOo77qc3X85nUpboafyjPF3UboafyjPF3UcTkUy15tIbrvfdTm6/nM6jvupzdfzmdSluhp/KM8XdRuhp/KM8XdRxORTLXm0huu991Obr+czqO+6nN1/OZ1KW6Gn8ozxd1G6Gn8ozxd1HE5FMtebSG6733U5uv5zOo77qc3X85nUpboafyjPF3UboafyjPF3UcTkUy15tIbrvfdTm6/nM6jvupzdfzmdSluhp/KM8XdRuhp/KM8XdRxORTLXm0huu991Obr+czqO+6nN1/OZ1KW6Gn8ozxd1I3P0/lGeLuo4nIplrzaQ3Xu/KnN1/OZ1KWpug1CvYqssxqs8L2K6NzZHNa5MK5ERSF7H6fyjPF3U6GPTIa2r1WQxpG11R71amVRVy/25/wDUeKeHm1LhYE8I+GaasIRj5Q+P1Wb9myrXhpy6pYk0+J6vSsyKOJVVXOf++iK5fWcq4XP8v5egwxtY1rWtRrGNRrWtTDWtRMIiJ7kwfRCUOiRIAAHFTkcVBFkOxP8Ar6vw/wC/l/W42BkOxf3jV/n5f1uNehjD9KnOe9H87QASDaZAJAEAkAQCQBAJAEAkAQCQBAJAEAkAQCQBAJAEAkAcVMhf/G6fyTvzebBTH6j+NUvk3fm854nlDqpyvnN9Y9mwQkhCTomAAAOKnI4qBkexf3jV/n5f1uNehkOxf3jV/n5f1uNehjD9P9dTnPej+doJABtMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIUx+o/jVL5N35vNgpj9R/GqXybvzeYn+OqnK+c31j2bBCSEJNpgAADipyOKgZHsX941f5+X9bjXoZDsX941f5+X9bjXoYw/T/XU5z3o/naCQAbTAAAAAAAAAAAAAAAAAAAAAAAAAAAAACFMfqP41S+Td+bzYKY/Ufxql8m783mJ/jqpyvnN9Y9mwQkhCTaYAAA4qcjioGR7F/eNX+fl/W416GQ7F/eNX+fl/W416GMP0/11Oc96P52gkAG0wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAhTH6j+NUvk3fm82CmP1H8apfJu/N5if46qcr5zfWPZsEJIQk2mAAAOKnI4uDyLI9i/vGr/AD8v63GvQx3Ypf8AH1f5+X9bjYIpjD9KrOe9H87QcgRkZNpkgjIyBIIyMgSCMjIEgjIyBIIyMgSCMjIEgjIyBIIyMgSCMjIEgjIyBIIyMgFMfqP41S+Td+bzXqpkLy/53U+Tf+chjE8odVOW9U31j2bBCSEJNpgAADg5Tre0l59encnZj0kFWaVm0mW7TGK5Mp70yhhdL7SaxNp7NQa2nNGnpnyVGxyRyqyNz2rsuRzsrlucbP8AcRF7s7rEFWxqqTypGsl+VWoq8VRHuTKcDRJ2to81H9StoMVDUq8VtKEC/tLVe5ZYY3P2kcrHI5cceLVTPvRDs93qPIVfIj6HKWWeH+f4rnxcDEj4poTV6wt0V97aHNx/Ub20Obj+pY3eo8hV8iPoN3qPIVfIj6HtMS7NcrabWGyvvbQ5uP6je2hzcf1LG71HkKvkR9Bu9R5Cr5EfQ9piXK5W02sNlfe2hzcf1G9tDm4/qWN3qPIVfIj6Dd6jyFXyI+gpiXK5W02sNlfe2hzcf1G9tDm4/qWN3qPIVfIj6Dd6jyFXyI+gpiXK5W02sNlfe2hzcf1G9tDm4/qWN3qPIVfIj6Dd6jyFXyI+gpiXK5W02sNlfe2hzcf1G9tDm4/qWN3qPIVfIj6Dd6jyFXyI+gpiXK5W02sNlfe2hzcf1G9tDm4/qWN3qPIVfIj6Dd6jyFXyI+gpiXK5W02sNlfe2hzcf1G9tDm4/qWN3qPIVfIj6Dd6jyFXyI+gpiXK5W02sNlfe2hzcf1G9tDm4/qWN3qPIVfIj6Dd6jyFXyI+gpiXK5W02sNlfe2hzcf1G9tDm4/qWN3qPIVfIj6Dd6jyFXyI+gpiXK5W02sNlfe2hzcf1G9tDm4/qWN3qPIVfIj6Dd6jyFXyI+gpiXK5W02sNlfe2hzcf1G9tDm4/qWN3qPIVfIj6Dd6jyFXyI+gpiXK5W02sNlZe1tDm4zPx6jFa1iq+GRHsbUkYrm8URyK5ceCoajd+jyFXyI+hVuwU9PhmtJVhjSCJz3OhiY16tRMq1qoie3+pmMs8aVi1Li4GHXwwjWMIw/2MPnpB3zTkefaPqGt3arbUTqUCTK99erPDJtLCr8MV8m0n8PFFRvFML7zc11crW7aIj9lNtrVVWo7HrIirjKZ9+DqkWAAB0fbJjnadqLWtVznUbLWtam05XLG5ERET2nnXZrWn1tGjpspW5L722Ymw/s72sasj3uar3u2W4w73Kq5X2HsCocVaBn+wulSUtPp15tn0scbttGrlEVz3P2c+/G1hfdlDRENQ5AQCQBAJAHHIyYD7Ttdt1H6W2rYWD9qsOikVGRyKqZjRFRHovs2l+BR0TtVZr6pepXb0dmtBCs37U9rIViRrWuVrthETPrYVF45b/YD04GT07t9QnfXjR00a2eFd08Lo2TLtI3EblTDuK44HyX7RtP9JNGi2HOruekzmQPeyJrHbLnvcn7rUX+JeAGxBlNO7e0bD4mtdLGkzZXRSzxOihkbFn0ite71Vxh2VzwxxPnD9oenPfGm3M2OWR0cVh8L215HtXCox6pheOPFANeRkyOp/aHp9aWxE90yurORszo4XPZGq4RMuTgnFUT+pXrdqI7GoUGR3LEbbVf0zKL6zWtlZsyKkjpVTbbwbtIiLhUanucBttpBtIedds9Zus1XTada2taK3H67kijlXa2nJlNtq+5EQ63s39obobF6tqNlLLorKR1568SLJOqu2NhrI0wvsRfj6ypx4YD1gGcqdsKsjLrv8Zi0Y/SWYponRzMbsq5F2F4+xqnXUftJ02d8LWvmT00voY3vhe1jpPV9Xa9mfXZ/TaT4gbPJJjW9oI2andhffk2K1Z08lR0DGwwsbHG90iTIm07g5Vx/5L8Czp/bqnO+sxrbLFtKqV3zwPjjmVEz6jlTDvans+IGpBjr32jafBNYges6yVnPSVGQuejEauHOVU93FOP8y3f7a0YK1Wy+R6w21RIEYxznuXHs2U4p8P6gaYGd7Pdr6uoSTRwJNtwJmX0sTo0YuVbsuVfYuUdwXj6q/A0QAEgCMHQ9stFW9RswJs7cjMx7Sq1qStVHNVVT+bUO/OKpkDzzs92vbVoQQ2KV1lmpE2s+vHXfKr1ixHtseiIxUXGeLk4oqceCrva0m2xjla5iua1ysfjbaqp+67CqmU9i8VOezj2ez4cf+Ic2pgDkAAAAAAAAAAAAA83+1fRZ7b9JSKu+eNll6zIxquRsbljRdrHsTCO8Dsu0PYyB1C3XoQMryyojk9H6npHN2V9G5y/wrsomM4/obRUyRsoB5FoukpKmlxTaZrHpYlh2pp7D46tdzERFljy5cJ6uUaiN9yIXOxek2IH9pHT6e+Vk8qOiglRGttN27Kq1qv4cUe3/AOk+J6irU/58CdkDx+HS78zf2KjVvUNNkqzssR3nNSOOdzXKiRP4v2VeiNdjOUe5dlMqq19L0NywVK1nStXfPFKxrv8AqXMpRuark9PG7aVqYzwwiJxXC/H2hWp9MHFGJ7QPCHS2Hy6+zM61JbLUtSUK7bTXRxo7ab6Z72o1dn28HcV93v77SaKz6volupBN3WzTUibM5qp6JGQzsRj1+KLhFXiiqvtVFNpZ7B6dIsquge1srtp8cU0sUT3KiIrlYxyNyuEyuOJ31KlHBG2OKNscTEwxjERrWpnPBE/mqqB57237PyXNZ0pzqr56TYkbYdsq6NqK964cqf2Pr2v0SxVZSZpleSOi2y112LTlWO3JHtovquRUVyYV6fvcFVPYh6NgjZQDxXS9GuLPrzm070dazpNlkLLauklklRrGxo5yqu07G1hMrhHKiKcNU7O3X9ntNrtpzOsx3ZnvgRi7bGOWfDlT3J67fE9u2UGz+eQPGbLbXf8AraU9lLndrUhV6pjbSKr7FXhtYzjPDKpk66XStRkk0dyU9VWeCRrrU9t8j27avZtLGiuVWphFyvDPw4Hruj9malOSaWCJyTTI1skj5HzOcjfYmXquP7fA7lWIB5dpWjWmXe073V5Gss17DYHq1UbK5drCMX3+1DJJXt06+gROhcyyy/YmbB6NZbW1tNVHJDlu03Cf7k4p7UPftlDqtX7O1bixuniV0kediVjnQysRUVFakjFRyIqKuUzhQMl9nU0TbWosetnvSVY7FpbcLK+1HlUYjGMe9Gom1xyuV2k+HD0RDpdF7NVKTpH14NiSRV25XOdLK9OHBXvVXY9VFxnB3SASAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA//Z",
                fit: BoxFit.fitWidth,
              ),
            ),
            Padding(padding: EdgeInsets.all(15)),
            Row(
              children: [
                Container(
                    height: 40,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "PANJANG:",
                          style: TextStyle(color: Colors.white),
                        )
                      ],
                    )),
                Padding(padding: EdgeInsets.all(5)),
                Expanded(
                  child: TextField(
                    decoration: InputDecoration(
                        fillColor: Colors.blue[400],
                        suffixText: "cm",
                        suffixStyle: TextStyle(color: Colors.white),
                        filled: true,
                        hintText: "Masukan Nilai Panjang",
                        hintStyle:
                            TextStyle(fontSize: 15, color: Colors.black38),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(40))),
                    onChanged: (txt) {
                      setState(() {
                        _panjang = double.parse(txt);
                      });
                    },
                    keyboardType: TextInputType.number,
                    maxLength: 3,
                  ),
                )
              ],
            ),
            Row(
              children: [
                Container(
                    height: 40,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("LEBAR:       ",
                            style: TextStyle(color: Colors.white))
                      ],
                    )),
                Padding(padding: EdgeInsets.all(5)),
                Expanded(
                  child: TextField(
                    decoration: InputDecoration(
                        fillColor: Colors.blue[400],
                        suffixText: "cm",
                        suffixStyle: TextStyle(color: Colors.white),
                        filled: true,
                        hintText: "Masukan Nilai Lebar",
                        hintStyle:
                            TextStyle(fontSize: 15, color: Colors.black38),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(40))),
                    onChanged: (txt) {
                      setState(() {
                        _lebar = double.parse(txt);
                      });
                    },
                    keyboardType: TextInputType.number,
                    maxLength: 3,
                  ),
                )
              ],
            ),
            ElevatedButton(
              style: ButtonStyle(
                shape: MaterialStateProperty.all(RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10))),
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => Hasil(
                            luas: _luas,
                            keliling: _keliling,
                            panjang: _panjang,
                            lebar: _lebar)));

                setState(() {
                  _luas = _panjang * _lebar;
                  _keliling = 2 * _panjang + 2 * _lebar;
                });
              },
              child: Text("HITUNG"),
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        child: Container(
          height: 40,
          color: Colors.black54,
          alignment: Alignment.center,
          child: Text(
            "Copyright @2021",
            style: TextStyle(fontSize: 15, color: Colors.grey),
          ),
        ),
      ),
    );
    return scaffold;
  }
}
